from PIL import Image

# Sets brightness - recommended value: 6
brightness = 6
# Sets amount of pixels per side 
res = 100
# Defines the range of the fractal 
radius = 5
x_min, x_max = -1*radius, radius
y_min, y_max = -1*radius, radius

# Create a blank image with a white background
width, height = res, res
image = Image.new("RGB", (width, height), (255, 255, 255))
pixels = image.load()

# Define the complex function to iterate on
def f(x): 
    return x**3 + 1

# Define the derivative of the function
def df(x):
    return 3*x**2

# Define the Newton function
def newton(z):
    for i in range(255):
        dz=df(z)
        if abs(dz) == 0:
            return 255
        z = z - f(z)/dz
        if abs(f(z)) < 1e-10:
            return i
    return 255

# Generate the fractal
for x in range(width):
    # Progress bar
    print(round((x/width)*100),'%')
    for y in range(height):
        zx = x * (x_max - x_min) / width + x_min
        zy = y * (y_max - y_min) / height + y_min

        c = complex(zx, zy)
        ni = int(newton(c) * brightness)
        pixels[x, y] = (ni, ni, ni)

# Show the image
image.show()
image.save('Newton-Fractal.jpg')