import matplotlib.pyplot as plt
import ast
import os
import natsort
import imageio.v2 as imageio

# For background Image 
img = plt.imread("C:\\Users\\Nedim\\Mandelbread.png")
fig, ax = plt.subplots()
ax.imshow(img, extent=[-2.5, 2.5, -2.5, 2.5])
fig.set_size_inches(12,12)


# Extracts data as one string from document - Add own file location
with open(r'D:\Users\Nedim\Downloads\orbitus.txt') as f:
    data = f.read().replace('\n', '')
main_list, substring_length = [], []

# Divides string into substrings, which it then adds to a main_list
while len(data) > 0:
    substring = data[data.find('['):data[data.find('['):].find(']') + 1]
    main_list.append(substring)
    data = data[data[data.find('['):].find(']') + 1:]
    substring_length.append(len(substring))
    if substring == "":
        break

# Iterate through each generation and save them as a new picture
for i in range(len(main_list)):
    print(i/len(main_list))
    main_list_one = ast.literal_eval(main_list[i])
    x_values = [x for x, y in main_list_one]
    y_values = [y for x, y in main_list_one]
    plt.scatter(x_values, y_values, marker='.', s=15)
    plt.savefig(f'E:\\VSCode\\IteratedRefinementAnimation\\{i}.png', dpi=300)
    y_values, x_values = [], []

# Make the Video
filenames = [os.path.join("E:\\VSCode\\IteratedRefinementAnimation", f) for f in os.listdir('E:\\VSCode\\IteratedRefinementAnimation') if f.endswith(".jpg") or f.endswith(".png")]
filenames = natsort.natsorted(filenames)
images = []
for filename in filenames:
        images.append(imageio.imread(filename))
imageio.mimsave('E:\\VSCode\\IteratedRefinementAnimation\\video.gif', images, duration=0.1)